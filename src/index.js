import React from 'react';
import ReactDOM from 'react-dom';
import 'animate.css';
import './styles/styles.scss';
import App from './App';

ReactDOM.render(
    <App />,
  document.getElementById('root')
);

