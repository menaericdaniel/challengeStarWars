import { type } from '@testing-library/user-event/dist/type';
import {types} from '../types/types';

export const search = (keywords) => {
  return async (dispatch) => {
    try {
      const response = await fetch(
        `https://swapi.dev/api/planets/?search=${keywords}`
      );
      const data = await response.json();
      dispatch(showResult({
        result : data.results,
        keywords 
      }))
    } catch (error) {
      console.log(error);
    }
  };
};

export const showResult = (data) => ({
    type : types.searchPlanets,
    payload : data
})

export const addFavorite = (planet) => ({
  type : types.addFavorite,
  payload : planet
})

export const removeFavorite = (planet) =>({
  type : types.removeFavorite,
  payload : planet
})
