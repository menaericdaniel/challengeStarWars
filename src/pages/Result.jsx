import React from "react";
import { useSelector } from "react-redux";
import { PlanetCard } from "../components/PlanetCard";

export const Result = () => {
  const { result, keywords } = useSelector((state) => state.planets.search);

  const {favorites} = useSelector(state => state.planets)

  return (
    <div>
      <h4 className="text-light">Resultado de tu búsqueda: {keywords}</h4>
      <hr  className="text-light"/>
      <div className="row ">
        {
          result.length === 0 
          ? <p className="text-warning">No hay planetas que coincidan con el criterio de búsqueda</p>
          :
          result.map((planet) => {
            for (let i = 0; i < favorites.length; i++) {
              if(favorites[i].name === planet.name){
                planet.favorite = true
              }
            }
          return <PlanetCard key={planet.created} {...planet} />
          })}
      </div>
    </div>
  );
};
