import React from 'react'
import { useSelector } from 'react-redux'
import { PlanetCard } from '../components/PlanetCard';

export const Favorites = () => {

    const {favorites} = useSelector(state => state.planets);

    return (
        <div>
      <h4 className='text-light'>Planetas favoritos</h4>
      <hr className='text-light mb-5' />
      <div className="row ">
        {
          favorites.length === 0 
          ? <p className='text-warning'>Aún no has agregado planetas favoritos</p>
          :
          favorites.map((planet) => {
          return <PlanetCard key={planet.created} {...planet} />
          }
          )}
      </div>
    </div>
    )
}
