import React, { useEffect, useMemo } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { PlanetCard } from "../components/PlanetCard";
import useCount from "../hooks/useCount";

export const Planets = () => {
  const [state, setState] = useState({
    data: null,
    loading: true,
    error: null,
    pages: [],
    countPages: 0,
    start: 1,
  });

  const { pages, data, loading, error, countPages, start } = state;

  const { current, previous, next, goStart, goFinish, goPage } = useCount({
    current: start,
    start: start,
    finish: countPages,
  });

  const { favorites } = useSelector((state) => state.planets);

  const getData = async () => {
    try {
      let response = await fetch(`https://swapi.dev/api/planets`);
      let data = await response.json();

      const countPages = data.count / 10;
      const pages = [];

      for (let i = 1; i <= countPages; i++) {
        pages.push(i);
      }

      setState({
        data,
        loading: false,
        error: null,
        pages,
        countPages,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useMemo(() => getData(), [loading]);

  const pageShow = async (url) => {
    try {
      let response = await fetch(url);
      let data = await response.json();

      setState({
        ...state,
        data,
      });
    } catch (error) {
      console.log(error);
    }
  };

  const handleGoPage = (e, page) => {
    e.preventDefault();
    goPage(page);
    pageShow(`https://swapi.dev/api/planets/?page=${page}`);
  };

  const handlePreviousPage = (e) => {
    e.preventDefault();
    current > 1 && previous()
    pageShow(data.previous);
  };

  const handleNextPage = (e) => {
    e.preventDefault();
    current < countPages && next()
    pageShow(data.next);
  };

  return (
    <div>
      <h4 className="text-light">Planetas</h4>
      <hr className="text-light"/>
      <nav className="d-flex justify-content-end mb-4">
        <ul className="pagination ">
          <li className="page-item ">
            <Link className="page-link" to="/" aria-label="Previous" onClick={handlePreviousPage}>
              <span aria-hidden="true">&laquo;</span>
            </Link>
          </li>
          {pages.map((page, index) => (
            <li
              className={`page-item ${page === current && "active"}`}
              key={page + index}
            >
              <a
                className="page-link"
                href="/"
                onClick={(event) => handleGoPage(event, page)}
              >
                {page}
              </a>
            </li>
          ))}
          <li className="page-item">
            <Link className="page-link" to="/" aria-label="Next" onClick={handleNextPage}>
              <span aria-hidden="true">&raquo;</span>
            </Link>
          </li>
        </ul>
      </nav>
      <div className="row ">
        {loading ? (
          <p>loading</p>
        ) : (
          data.results.map((planet) => {
            for (let i = 0; i < favorites.length; i++) {
              if (favorites[i].name === planet.name) {
                planet.favorite = true;
              }
            }
            return <PlanetCard key={planet.created} {...planet} />;
          })
        )}
      </div>
    </div>
  );
};
