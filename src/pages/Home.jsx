import React from 'react';

const images = require.context('../assets/images')

export const Home = () => {
    return (
        <div className='home__main d-flex justify-content-center animate__animated animate__zoomIn animate__slower'>
           <img src={images('./logo.png')} alt="" className='w-75 rounded-2 border'/>
        </div>
    )
}
