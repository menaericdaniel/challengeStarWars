import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Navbar } from '../components/Navbar'
import { Home } from '../pages/Home'
import { PlanetsRouter } from './PlanetsRouter'

export const AppRouter = () => {
    return (
        <BrowserRouter>
            <Navbar/>
            <div className="container">
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/planets/*" element={<PlanetsRouter/>}/>
                </Routes>
            </div>
        </BrowserRouter>
    )
}
