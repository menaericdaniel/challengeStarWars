import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { Favorites } from '../pages/Favorites'
import { Planets } from '../pages/Planets'
import { Result } from '../pages/Result'

export const PlanetsRouter = () => {
    return (
        <Routes>
            <Route path="all" element={<Planets/>}/>
            <Route path="favorites" element={<Favorites/>}/>
            <Route path='result' element={<Result/>}/>
            
        </Routes>
    )
}
