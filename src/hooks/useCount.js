import { useState } from "react";

function useCount(initialState) {
  const [state, setState] = useState(initialState);

  const { current, start, finish } = state;

  const next = () =>
    setState({
      ...state,
      current: current + 1,
    });
  const previous = () =>
    setState({
      ...state,
      current: current - 1,
    });
  const goStart = () =>
    setState({
      ...state,
      current: start,
    });
  const goFinish = () =>
    setState({
      ...state,
      current: finish,
    });
  const goPage = (page) =>
    setState({
      ...state,
      current: page,
    });

  return {
    current,
    next,
    previous,
    goStart,
    goFinish,
    goPage,
  };
}

export default useCount;
