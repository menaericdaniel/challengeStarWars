import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addFavorite, removeFavorite } from "../actions/planets";

const planetsImages = require.context("../assets/images/planets");

export const PlanetCard = (props) => {
  const { name, diameter, climate, terrain, favorite } = props;

  const [favoriteView, setFavoriteView] = useState(favorite);

  const dispatch = useDispatch();

  const handleClickFavorite = (e) => {
    e.preventDefault();

    if (favoriteView) {
      const favorite = {
        ...props,
        favorite: false,
      };
      dispatch(removeFavorite(favorite));
    } else {
      const favorite = {
        ...props,
        favorite: true,
      };
      dispatch(addFavorite(favorite));
    }
    setFavoriteView(!favoriteView);
  };

  return (
    <div className="col-12 col-md-4 col-lg-3 g-2">
      <div className="card">
        <div className="box-image">
          <img
            src={planetsImages(`./${name}.png`)}
            alt={name}
            className="animate__animated animate__fadeIn animate__slower"
          />
        </div>
        <div className="body">
          <div className=" d-flex justify-content-between px-3">
            <h4 className="card-title">{name}</h4>
            <a href="/" className="h3" onClick={handleClickFavorite}>
              {favoriteView ? (
                <i className="fas fa-heart text-danger" ></i>
              ) : (
                <i className="far fa-heart"></i>
              )}
            </a>
          </div>

          <ul className="list-group list-group-flush">
            <li className="list-group-item animate__animated animate__fadeInRight">
              <b>Clima: </b>
              {climate}
            </li>
            <li className="list-group-item animate__animated animate__fadeInRight animate__delay-1s">
              <b>Diámetro: </b>
              {diameter}
            </li>
            <li className="list-group-item animate__animated animate__fadeInRight animate__delay-2s">
              <b>Terreno: </b>
              {terrain}
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
