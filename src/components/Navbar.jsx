import { search } from "../actions/planets";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import useForm from "../hooks/useForm";

export const Navbar = () => {
  const [{ keywords }, handleInputChange, reset] = useForm({
    keywords: "",
  });

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleSearch = async (e) => {
    e.preventDefault();
    if (keywords !== "") {
      dispatch(search(keywords));
      reset();
      navigate("/planets/result");
    }
  };

  return (
    <header className="position-sticky top-0 mb-4" style={{ zIndex: 100 }}>
      <nav className="navbar navbar-dark bg-dark">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            StarWars App Challenge
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasNavbar"
            aria-controls="offcanvasNavbar"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="offcanvas offcanvas-end"
            tabIndex="-1"
            id="offcanvasNavbar"
            aria-labelledby="offcanvasNavbarLabel"
          >
            <div className="offcanvas-header">
              <h5 className="offcanvas-title" id="offcanvasNavbarLabel">
                StarWars App Challenge
              </h5>
              <button
                type="button"
                className="btn-close text-reset"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
              ></button>
            </div>
            <div className="offcanvas-body bg-dark">
              <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
                <li className="nav-item">
                  <Link className="nav-link active" aria-current="page" to="/">
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/planets/all">
                    Planetas
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/planets/favorites">
                    Planetas favoritos
                  </Link>
                </li>
              </ul>
              <form className="d-flex mt-2" onSubmit={handleSearch}>
                <input
                  className="form-control me-2"
                  type="search"
                  name="keywords"
                  aria-label="keywords"
                  onChange={handleInputChange}
                  value={keywords}
                />
                <button className="btn btn-outline-light" type="submit">
                  Buscar
                </button>
              </form>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};
