import { types } from "../types/types";

const initialState = {
    search : {
        result : [],
        keywords : ""
    },
    favorites : []
}

export const planetsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.searchPlanets :
            return {
                ...state,
                search : action.payload
            }
        case types.addFavorite :
            return {
                ...state,
                favorites : [...state.favorites, action.payload]
            }
        case types.removeFavorite :
            return {
                ...state,
                favorites : state.favorites.filter(planet => planet.name !== action.payload.name)
            }
        default:
            return state;
    }
}