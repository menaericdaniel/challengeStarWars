# Challenge Star Wars - ReactJS
## Descripción

El challenge consiste en desarrollar una app con React integrándola con Redux o graphql, que permita (usando el API REST de Star Wars [1] para usar con redux o [3] para graphql) listar todos los planetas en una lista con un paginado de a 10 planetas con las siguientes columnas en las cards: nombre, diametro, clima y terreno.
La vista debe permitir buscar por nombre de planeta usando un input y un botón cada vez que se hace un click en buscar o apretar enter.
La lista de planetas debe permitir también guardar uno o más planetas como favoritos en el estado y por último, crear una vista para ver los favoritos que hemos guardado.


La idea es subirlo a un repo en github y compartir el link una vez terminado.

#### React:
- componentes funcionales y hooks
- usar context api si fuera necesario
#### Maquetado:
- Sobre la parte gráfica presentarlo a libre criterio
- usar flexbox o grid
- styled components o sass

*Será un plus si agregas:*
- unit testing
- typescript
- graphql
- async/await
- children props

#### Links:
- [Documentación de la API](https://swapi.dev/documentation)
- [Buscador](https://swapi.dev/documentation#search)
- [Graphql](https://graphql.org/swapi-graphql/)
## Dependencias a instalar
### [react-router-dom](https://reactrouter.com/docs/en/v6)
~~~
yarn add react-router-dom
~~~
### [redux](https://es.redux.js.org/) 
~~~
yarn add redux
~~~
### [react-redux](https://react-redux.js.org/)
~~~
yarn add react-redux
~~~
### [redux-thunk](https://www.npmjs.com/package/redux-thunk) 
~~~
yarn add redux-thunk
~~~
### [sass](https://sass-lang.com/documentation)
~~~
yarn add sass -D
~~~
## Configuración

